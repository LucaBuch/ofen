#pragma once
#include <inttypes.h>

struct Oven_State
{
    uint16_t temp_1_is;
    uint16_t temp_1_should;
    uint16_t temp_2_is;
    uint16_t temp_2_should;

    bool upper_1_on;
    bool lower_1_on;
    bool upper_2_on;
    bool lower_2_on;
};
