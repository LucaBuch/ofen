CHIP = attiny45
PROG = avrdragon
F_CPU = 8000000
MAIN = main
C_SRC = 
CPP_SRC = $(MAIN).cpp
INCLUDE_DIRS = 
OBJ = 


C_FLAGS = -gdwarf-2
C_FLAGS += -DF_CPU=$(F_CPU)UL
C_FLAGS += -Os
C_FLAGS += -funsigned-char
C_FLAGS += -funsigned-bitfields
C_FLAGS += -fpack-struct
C_FLAGS += -fshort-enums
C_FLAGS += -Wall
C_FLAGS += -Wstrict-prototypes
C_FLAGS += -Wa,-adhlns=$(<:%.c=%.lst)
C_FLAGS += $(patsubst %,-I%,$(INCLUDE_DIRS))
C_FLAGS += -std=gnu99

CPP_FLAGS = -gdwarf-2
CPP_FLAGS += -DF_CPU=$(F_CPU)UL
CPP_FLAGS += -Os
CPP_FLAGS += -funsigned-char
CPP_FLAGS += -funsigned-bitfields
CPP_FLAGS += -fpack-struct
CPP_FLAGS += -fshort-enums
CPP_FLAGS += -fno-exceptions
CPP_FLAGS += -Wall
CPP_FLAGS += -Wundef
CPP_FLAGS += -Wa,-adhlns=$(<:%.cpp=%.lst)
CPP_FLAGS += $(patsubst %,-I%,$(INCLUDE_DIRS))
CPP_FLAGS += -std=c++14



all: gccversion elf hex eep lss sym
	@echo Compilation done.
	@echo
elf: $(MAIN).elf
hex: $(MAIN).hex
eep: $(MAIN).eep
lss: $(MAIN).lss
sym: $(MAIN).sym

%.hex: %.elf
	avr-objcopy -O ihex -R .eeprom -R .fuse -R .lock $< $@

%.eep: %.elf
	avr-objcopy -j .eeprom --set-section-flags=.eeprom="alloc,load" --change-section-lma .eeprom=0 --no-change-warnings -O ihex $< $@ || exit 0

%.lss: %.elf
	avr-objdump -h -S -z $< > $@

%.sym: %.elf
	avr-nm -n $< > $@


#---------------------------------- LINKER ----------------------------------- 
OBJ += $(C_SRC:%.c=%.o) $(CPP_SRC:%.cpp=%.o)
.SECONDARY: $(MAIN).elf
.PRECIOUS: $(OBJ)
%.elf: $(OBJ)
	avr-gcc -mmcu=$(CHIP) -I. $(C_FLAGS) -MMD -MP -MF ".dep/$(@F).d" $^ --output $@ -Wl,-Map=$(MAIN).map,--cref -lm


#---------------------------------- COMPILER --------------------------------- 
# C
%.o: %.c
	avr-gcc -c -mmcu=$(CHIP) -I. $(C_FLAGS) -MMD -MP -MF ".dep/$(@F).d" $< -o $@ 

# C++
%.o: %.cpp
	avr-gcc -c -mmcu=$(CHIP) -I. -x c++ $(CPP_FLAGS) -MMD -MP -MF ".dep/$(@F).d" $< -o $@ 


#---------------------------------- VERSION ---------------------------------- 
.PHONY: gccversion
gccversion:
	avr-gcc --version
	mkdir -p .dep


#----------------------------------- FLASH ----------------------------------- 
.PHONY: flash
flash:
	avrdude -p $(CHIP) -c $(PROG) -P usb -U flash:w:$(MAIN).hex


#----------------------------------- CLEAN ----------------------------------- 
.PHONY: clean
clean:
	rm -f *.o
	rm -f *.hex
	rm -f *.elf
	rm -f *.eep
	rm -f *.lss
	rm -f *.sym
	rm -f *.lst
	rm -f *.map
	rm -rf .dep


#--------------------------------- CLEAN RUN ---------------------------------
cleanrun: clean all flash
	@echo
	@echo Process done.
	@echo

#--------------------------------- FAST RUN ----------------------------------
fastrun: all flash
	@echo
	@echo Process done.
	@echo
