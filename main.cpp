#define __AVR_ATmega32__

#include <avr/io.h>
#include <util/delay.h>
#include <util/twi.h>
#include <inttypes.h>
#include <util/delay.h>

//#include "mcp.h"
#include "oven_state.h"
#define MCP23017_ADDR 0x40
#define MCP_GPIO_A 0x12
#define MCP_GPIO_B 0x13

namespace I2C_Handler
{
    void i2c_send(uint8_t twdr_val, uint8_t check_against)
    {
        TWDR = twdr_val;
        TWCR = _BV(TWINT) | _BV(TWEN); //Start send
        while (!(TWCR & (_BV(TWINT))))
            ; // Wait for end send signal on twint
        if ((TWSR & check_against) != check_against)
        {
            // Eventually error handling?
        }
        return;
    }

    void i2c_write(uint8_t address, uint8_t reg, uint8_t data)
    {
        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN); //Enable and start I2C and reset interrupt flag
        while (!(TWCR & (_BV(TWINT))))
            ; // Wait for start to send

        i2c_send(address, TW_MT_SLA_ACK);
        i2c_send(reg, TW_MT_DATA_ACK);
        i2c_send(data, TW_MT_DATA_ACK);

        TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
        return;
    }

    uint8_t i2c_read(uint8_t address, uint8_t reg)
    {
        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN); //ENable and start I2C
        while (!(TWCR & (_BV(TWINT))))
            ; // Wait for start to send
        if ((TWSR & 0xF8) != TW_START)
        {
            // Eventually error handling?
        }

        i2c_send(reg, TW_MT_DATA_ACK);
        TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);

        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN); //Reenable and restart I2C
        i2c_send(address | 0x01, TW_MR_SLA_ACK);    // Send address + READ bit
        uint8_t data_read = TWDR;
        TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
        return data_read;
    }
}

Oven_State state;

void update_led()
{
    uint8_t current_A = I2C_Handler::i2c_read(MCP23017_ADDR, MCP_GPIO_A);
    if (state.lower_1_on || state.upper_1_on)
    {
        I2C_Handler::i2c_write(MCP23017_ADDR, MCP_GPIO_A, current_A |= 0x40);
    }
    else
    {
        I2C_Handler::i2c_write(MCP23017_ADDR, MCP_GPIO_A, current_A &= ~(0x40));
    }
    if (state.lower_2_on || state.upper_2_on)
    {
        I2C_Handler::i2c_write(MCP23017_ADDR, MCP_GPIO_A, current_A |= 0x80);
    }
    else
    {
        I2C_Handler::i2c_write(MCP23017_ADDR, MCP_GPIO_A, current_A &= ~(0x80));
    }
}

void setup_mcp()
{
    I2C_Handler::i2c_write(MCP23017_ADDR, 0x00, 0x30); //Setting IO dir of bank A
    I2C_Handler::i2c_write(MCP23017_ADDR, 0x01, 0xF0); //Setting IO dir of bank B
}

int main()
{
    SREG |= _BV(7); // Globally enable Interrupts by setting GIE bit in SREG
    TWCR |= _BV(TWIE);
    setup_mcp();
    while (true)
    {
        state.lower_1_on = true;
        state.upper_1_on = false;
        state.lower_2_on = false;
        state.upper_2_on = false;
        update_led(); //should enable LED 1 and disable LED 2
        state.lower_1_on = false;
        _delay_ms(1000);
        update_led(); //both LEDs should be off by now
    }
}

void digital_write(uint8_t port, uint8_t pin, bool value)
{
    if (value)
    {
        port |= _BV(pin);
    }
    else
    {
        port &= ~(_BV(pin));
    }
    return;
}
