#pragma once
#define __AVR_ATmega32__

#include <avr/io.h>
#include <util/delay.h>
#include <util/twi.h>
#include <inttypes.h>

namespace I2C_Handler{
    void i2c_write(uint8_t address, uint8_t data, uint8_t reg);
    uint8_t i2c_read(uint8_t address, uint8_t reg);
    void i2c_send(uint8_t twdr_val, uint8_t check_against);
};