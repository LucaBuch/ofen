#include "mcp.h"
namespace I2C_Handler{
    void i2c_send(uint8_t twdr_val, uint8_t check_against)
    {
        TWDR = twdr_val;
        TWCR = _BV(TWINT) | _BV(TWEN); //Start send
        while (!(TWCR & (_BV(TWINT))))
            ; // Wait for end send signal on twint
        if ((TWSR & check_against) != check_against)
        {
            // Eventually error handling?
        }
        return;
    }
    
    void i2c_write(uint8_t address, uint8_t data, uint8_t reg)
    {
        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN); //Enable and start I2C and reset interrupt flag
        while (!(TWCR & (_BV(TWINT))))
            ; // Wait for start to send

        i2c_send(address, TW_MT_SLA_ACK);
        i2c_send(reg, TW_MT_DATA_ACK);
        i2c_send(data, TW_MT_DATA_ACK);

        TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
        return;
    }

    uint8_t i2c_read(uint8_t address, uint8_t reg)
    {
        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN); //ENable and start I2C
        while (!(TWCR & (_BV(TWINT))))
            ; // Wait for start to send
        if ((TWSR & 0xF8) != TW_START)
        {
            // Eventually error handling?
        }

        i2c_send(reg, TW_MT_DATA_ACK);
        TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);

        TWCR = _BV(TWINT) | _BV(TWSTA) | _BV(TWEN); //Reenable and restart I2C
        i2c_send(address | 0x01, TW_MR_SLA_ACK);    // Send address + READ bit
        uint8_t data_read = TWDR;
        TWCR = _BV(TWINT) | _BV(TWEN) | _BV(TWSTO);
        return data_read;
    }
}